#include "termios.h"

#include <string>
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>


#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_broadcaster.h>

#include "serialcomm_s300.h"

struct DEVICE {
	char *device_name_;
	int baud_rate_;
	SerialCommS300 serial_comm_;

    int connected_;
    std::string connect_cmd_;

};


std::vector<DEVICE> device_list;
tf::Transform transform_laser_1;
tf::Transform transform_laser_2;


class SickS300 {

public:

	SickS300();
	~SickS300();

	void update();
	void broadcast_transform();

public: 

	float *ranges_1;
	float *ranges_2;

	int check_device_1;
	int check_device_2;

	sensor_msgs::LaserScan scan_data_;
	sensor_msgs::LaserScan scan_data_2;

	ros::Publisher scan_data_publisher_;
	ros::Publisher scan_data_publisher_2;
	
	//Publish 2*laser("scan_1" "scan_2"):
	sensor_msgs::LaserScan scan_1_data_;
	sensor_msgs::LaserScan scan_2_data_;
	ros::Publisher         scan_1_publisher_;
    ros::Publisher         scan_2_publisher_;

	//==========================================//
    tf::TransformBroadcaster tf_broadcaster_;
    tf::TransformBroadcaster tf_broadcaster_2;

    tf::Vector3 transform_vector_laser_1_;
    tf::Vector3 transform_vector_laser_2_;


    //! Scan field of view parameters
	unsigned int field_of_view_;
	unsigned int start_scan_;
	unsigned int end_scan_;

    //! Send Transform or not
    bool send_transform_;
 
    ros::NodeHandle nodeHandle_;

    std::string frame_id_1_;
    std::string frame_id_2_;

};
