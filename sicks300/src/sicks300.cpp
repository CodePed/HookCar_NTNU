

#include "sicks300.h"



SickS300::SickS300()
{
	check_device_1 = 0;
	check_device_2 = 0;

    transform_laser_1.setOrigin(tf::Vector3(0.51078, 0.33568, 0.0));
    tf::Quaternion q_1;
    q_1.setRPY(0, 0, 0.8028507778);
    transform_laser_1.setRotation(q_1);


    transform_laser_2.setOrigin(tf::Vector3(-0.51078, -0.33568, 0.0));
    tf::Quaternion q_2;
    q_2.setRPY(0, 0, -2.338739222);
    transform_laser_2.setRotation(q_2);

	send_transform_ = 1;

	double fov = 270.0;

	if ((fov > 270) || (fov < 0)) {
		ROS_WARN("S300 field of view parameter set out of range (0-270). Assuming 270.");
		fov = 270.0;
	}

	field_of_view_ = (unsigned int) (fov * 2.0); // angle increment is .5 degrees
    field_of_view_ <<= 1;
    field_of_view_ >>= 1; // round to a multiple of two
    start_scan_ = 270 - field_of_view_ / 2;
    end_scan_   = 270 + field_of_view_ / 2;

    frame_id_1_ = std::string("base_laser_link");
    frame_id_2_ = std::string("base_laser_link_2");

    scan_data_.header.frame_id = frame_id_1_;
	scan_data_.angle_min = (float) (-(field_of_view_ / 4.0) / 180. * M_PI);
	scan_data_.angle_max = (float) ((field_of_view_ / 4.0) / 180. * M_PI);
	scan_data_.angle_increment = (float) (0.5 / 180. * M_PI);
	scan_data_.time_increment = 0;
	scan_data_.scan_time = 0.08;
	scan_data_.range_min = 0.1;
	scan_data_.range_max = 29.0;
	if(device_list.size() == 2){ //device_num = 2
		scan_data_.ranges.resize(field_of_view_*2);
		scan_data_.intensities.resize(field_of_view_*2);
		
		//Publish 2*laser("scan_1" "scan_2"):
		//Scan-1:
		scan_1_data_.header.frame_id = frame_id_1_;
	    scan_1_data_.angle_min       = scan_data_.angle_min;
	    scan_1_data_.angle_max       = scan_data_.angle_max;
	    scan_1_data_.angle_increment = scan_data_.angle_increment;
	    scan_1_data_.time_increment  = scan_data_.time_increment;
	    scan_1_data_.scan_time       = scan_data_.scan_time;
	    scan_1_data_.range_min       = scan_data_.range_min;
	    scan_1_data_.range_max       = scan_data_.range_max;
		scan_1_data_.ranges.resize(field_of_view_);
		scan_1_data_.intensities.resize(field_of_view_);
		scan_1_publisher_ = nodeHandle_.advertise<sensor_msgs::LaserScan>("scan_1", 10);
		//Scan-2:
		scan_2_data_.header.frame_id = frame_id_2_;
	    scan_2_data_.angle_min       = scan_data_.angle_min;
	    scan_2_data_.angle_max       = scan_data_.angle_max;
	    scan_2_data_.angle_increment = scan_data_.angle_increment;
	    scan_2_data_.time_increment  = scan_data_.time_increment;
	    scan_2_data_.scan_time       = scan_data_.scan_time;
	    scan_2_data_.range_min       = scan_data_.range_min;
	    scan_2_data_.range_max       = scan_data_.range_max;
		scan_2_data_.ranges.resize(field_of_view_);
		scan_2_data_.intensities.resize(field_of_view_);
		scan_2_publisher_ = nodeHandle_.advertise<sensor_msgs::LaserScan>("scan_2", 10);
	}
	else{ //device_num = 1
		scan_data_.ranges.resize(field_of_view_);
		scan_data_.intensities.resize(field_of_view_);
		
	    //Publish 1*laser("scan_1" ):
		scan_1_data_.header.frame_id = frame_id_1_;
	    scan_1_data_.angle_min       = scan_data_.angle_min;
	    scan_1_data_.angle_max       = scan_data_.angle_max;
	    scan_1_data_.angle_increment = scan_data_.angle_increment;
	    scan_1_data_.time_increment  = scan_data_.time_increment;
	    scan_1_data_.scan_time       = scan_data_.scan_time;
	    scan_1_data_.range_min       = scan_data_.range_min;
	    scan_1_data_.range_max       = scan_data_.range_max;
		scan_1_data_.ranges.resize(field_of_view_);
		scan_1_data_.intensities.resize(field_of_view_);
		scan_1_publisher_ = nodeHandle_.advertise<sensor_msgs::LaserScan>("scan_1", 10);
	}

	scan_data_publisher_ = nodeHandle_.advertise<sensor_msgs::LaserScan>("scan", 10);
	
}


SickS300::~SickS300()
{

}


void SickS300::update()
{
	unsigned int numRanges_1;
	unsigned int numRanges_2;
	unsigned int scanNum_1;
	unsigned int scanNum_2;

	for ( int i=0; i < device_list.size(); i++ ){

		if( device_list[i].connected_ != 0 ) {
			ROS_INFO_STREAM("Executing connect cmd " << device_list[i].connect_cmd_ << "...");
			ROS_INFO("size %d",device_list.size());

			system(device_list[i].connect_cmd_.c_str());

			ROS_INFO("Opening connection to Sick300-laser %s",device_list[i].device_name_);

			device_list[i].connected_ = device_list[i].serial_comm_.connect((device_list[i].device_name_), device_list[i].baud_rate_);

			if (device_list[i].connected_ == 0) {
				ROS_INFO("Sick300 connected. %s",device_list[i].device_name_);
			} 
			else {
				ROS_ERROR("Sick300 not connected, will try connecting again in 500 msec... %s",device_list[i].device_name_);
				std::this_thread::sleep_for(std::chrono::milliseconds(500));
			}
		}
	}
	
	int status_1 = -1;
	int status_2 = -1;			
	if(device_list[0].connected_ == 0){
		status_1 = device_list[0].serial_comm_.readData();
	    if (status_1 == -2) {
	      ROS_ERROR("Error in communication(Device 1), closing connection and waiting 500 msec...");
	      device_list[0].serial_comm_.disconnect();
	      std::this_thread::sleep_for(std::chrono::milliseconds(500));
	      device_list[0].connected_ = -1;
	    } 		
	}
	if(device_list.size() == 2){
		if(device_list[1].connected_ == 0){
			status_2 = device_list[1].serial_comm_.readData();
	    	if (status_2 == -2) {
	    		ROS_ERROR("Error in communication(Device 2), closing connection and waiting 500 msec...");
	    		device_list[1].serial_comm_.disconnect();
	    		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	    		device_list[1].connected_ = -1;
	    	} 
		}
	}

	if(status_1 == 0){
		ranges_1 = device_list[0].serial_comm_.getRanges();
		numRanges_1 = device_list[0].serial_comm_.getNumRanges();
		
		check_device_1 = 1;
	}
	if(status_2 == 0){
	    ranges_2 = device_list[1].serial_comm_.getRanges();
	    numRanges_2 = device_list[1].serial_comm_.getNumRanges();

	    check_device_2 = 1;	    
	}

	if(device_list.size() == 2){
		if(check_device_1 && check_device_2){
			int store_index = 0;
            for (unsigned int i = end_scan_-1; i > start_scan_; i--) {//i=539~0
               	scan_data_.ranges[store_index]      = ranges_1[i];
                scan_1_data_.ranges[(end_scan_-1)-i]= ranges_1[i];
               	store_index += 1;
            }
            for (unsigned int i = end_scan_-1; i > start_scan_; i--) {//i=539~0
               	scan_data_.ranges[store_index]      = ranges_2[i];
                scan_2_data_.ranges[(end_scan_-1)-i]= ranges_2[i];
               	store_index += 1;
            }
            scan_data_.header.stamp = device_list[0].serial_comm_.getReceivedTime();
			scan_data_publisher_.publish(scan_data_);
			
			//Publish 2*laser("scan_1" "scan_2")
			scan_1_data_.header.stamp = device_list[0].serial_comm_.getReceivedTime();
			scan_2_data_.header.stamp = device_list[1].serial_comm_.getReceivedTime();
            scan_1_publisher_.publish(scan_1_data_);
			scan_2_publisher_.publish(scan_2_data_);
			
			check_device_1 = 0;
			check_device_2 = 0;
		}
	}
	else{
		int store_index = 0;
		if(status_1 == 0){
            for (unsigned int i = end_scan_-1; i > start_scan_; i--) {
               	scan_data_.ranges[store_index]  = ranges_1[i];
				scan_1_data_.ranges[store_index]= ranges_1[i];
               	store_index += 1;
            }
            scan_data_.header.stamp = device_list[0].serial_comm_.getReceivedTime();
            scan_data_publisher_.publish(scan_data_);
			
			//2020-0909
			scan_1_data_.header.stamp = device_list[0].serial_comm_.getReceivedTime();
            scan_1_publisher_.publish(scan_1_data_);			
		}
	}

}


void SickS300::broadcast_transform() {
	
  if (send_transform_) {
  	tf_broadcaster_.sendTransform(tf::StampedTransform( transform_laser_1, ros::Time::now(), "base_link", frame_id_1_));
  	tf_broadcaster_.sendTransform(tf::StampedTransform( transform_laser_2, ros::Time::now(), "base_link", frame_id_2_));
  }
  
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "cmc-sicks300");
	ros::Time::init();
	if(argc < 3){
		ROS_INFO("usage: <IP address> [<port number>] .....[<baud rate>]");
		return 0;
	}
	else{		
		if(argc == 3) {
			DEVICE dev_buf;
			dev_buf.device_name_ = argv[1];
			dev_buf.baud_rate_   = std::atoi(argv[argc-1]);
			dev_buf.connected_ = -1;
			device_list.push_back(dev_buf);
		}
		else if(argc == 4) {
			DEVICE dev_buf_1;
			dev_buf_1.device_name_ = argv[1];
			dev_buf_1.baud_rate_   = std::atoi(argv[argc-1]);
			dev_buf_1.connected_ = -1;
			device_list.push_back(dev_buf_1);

			DEVICE dev_buf_2;
			dev_buf_2.device_name_ = argv[2];
			dev_buf_2.baud_rate_   = std::atoi(argv[argc-1]);
			dev_buf_2.connected_ = -1;
			device_list.push_back(dev_buf_2);
		}
		else {
			ROS_INFO("usage: [<Device name>] .....[<Baud rate>]");
			return 0;
		}
	}

	ROS_INFO("Num of Device: %d", device_list.size());
	for(int i=0; i<device_list.size(); i++){
		//ROS_INFO("Device[%d] name: %s", i, device_list[i].device_name_);
		//ROS_INFO("Device[%d] baud rate: %d", i, device_list[i].baud_rate_);
	}

    SickS300 sickS300;
	while (ros::ok()) {
		sickS300.update();
		sickS300.broadcast_transform();
		ros::spinOnce();
	}

	ROS_INFO("Laser shut down.");

	return 0;

}
