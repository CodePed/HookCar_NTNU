#include "Motor.h"
#define SERIALPORT "/dev/ttyS0"

Motor::Motor(string dev_name,int Baudrate)
{
    samplingTime = 0.1;         // Sampling period 0.1 sec;
    ROS_INFO("CMC-AGV mode-robot");

    //----- Open Serial -----//
    speed_t baudrate = setBaudrate(Baudrate);
    serial->ok = connectSerial(serial,dev_name,baudrate);
    if(serial->ok)
    {
        ROS_WARN("Serial-port OK !");
    }
    else
    {
        ROS_ERROR("Serial-port Failed !");
    }

    //----- Create a ROS Timer for Calculating Odometry -----//
    timer_calculateOdom = nh.createTimer(ros::Duration(samplingTime),std::bind(&Motor::calculateOdom,this));

//    recieve_thread = new boost::thread(boost::bind(&Motor::readEncoder,this,0.01));

}
Motor::~Motor()
{
    ROS_INFO("Shut Down Motor.");
//    if(receive_thread)
//    {
//        delete receive_thread;
//    }
}

speed_t Motor::setBaudrate(int baudrate)
{
    switch(baudrate)
    {
        case 115200:   return B1152000;
        case 134:   return B134;
        default:    return B0;
    }

}

void Motor::calculateOdom()
{
    /*
    current_time = ros::Time::now();

    float a = 0.32;
    float b = 0.22;

    float x_w2 = -1*a;
    float y_w2 = b;

    float x_w3 = -1*a;
    float y_w3 = -1*b;

    float x_w4 = a;
    float y_w4 = -1*b;

    //----- Wheel Velocity -----//
    Rev_odom_V1 = (Rev_FL_rpm/(60.0*22.5))*2*M_PI*(0.15/2.0);
    Rev_odom_V2 = (Rev_RL_rpm/(60.0*22.5))*2*M_PI*(0.15/2.0);
    Rev_odom_V3 = (Rev_RR_rpm/(60.0*22.5))*2*M_PI*(0.15/2.0);
    Rev_odom_V4 = (Rev_FR_rpm/(60.0*22.5))*2*M_PI*(0.15/2.0);

    //----- Wheel Orientation -----//
    Rev_odom_t1 = Rev_FL_deg*M_PI/180.0;
    Rev_odom_t2 = Rev_RL_deg*M_PI/180.0;
    Rev_odom_t3 = Rev_RR_deg*M_PI/180.0;
    Rev_odom_t4 = Rev_FR_deg*M_PI/180.0;

    //----- Normalize Orientation Angle in radians -----//
    NormalizeAngle(Rev_odom_t1);
    NormalizeAngle(Rev_odom_t2);
    NormalizeAngle(Rev_odom_t3);
    NormalizeAngle(Rev_odom_t4);

    //test for error odom:Add Err_rate
    float Err_rate_vx=1.0;
    float Err_rate_vy=1.0;
    Rev_odom_vx = Err_rate_vx*(cos(Rev_odom_t1)*Rev_odom_V1+cos(Rev_odom_t2)*Rev_odom_V2+cos(Rev_odom_t3)*Rev_odom_V3+cos(Rev_odom_t4)*Rev_odom_V4)/4;
    Rev_odom_vy = Err_rate_vy*(sin(Rev_odom_t1)*Rev_odom_V1+sin(Rev_odom_t2)*Rev_odom_V2+sin(Rev_odom_t3)*Rev_odom_V3+sin(Rev_odom_t4)*Rev_odom_V4)/4;
    Rev_odom_v = sqrt(Rev_odom_vx*Rev_odom_vx + Rev_odom_vy*Rev_odom_vy);

    float W1 = (-y_w1*cos(Rev_odom_t1) + x_w1*sin(Rev_odom_t1))/(4*x_w1*x_w1 + 4*y_w1*y_w1);
    float W2 = (-y_w2*cos(Rev_odom_t2) + x_w2*sin(Rev_odom_t2))/(4*x_w2*x_w2 + 4*y_w2*y_w2);
    float W3 = (-y_w3*cos(Rev_odom_t3) + x_w3*sin(Rev_odom_t3))/(4*x_w3*x_w3 + 4*y_w3*y_w3);
    float W4 = (-y_w4*cos(Rev_odom_t4) + x_w4*sin(Rev_odom_t4))/(4*x_w4*x_w4 + 4*y_w4*y_w4);

    Rev_odom_w = W1*Rev_odom_V1 + W2*Rev_odom_V2 + W3*Rev_odom_V3 + W4*Rev_odom_V4;

    //compute odometry in a typical way given the velocities of the robot
    double dt       = (current_time - last_time).toSec();
    double delta_x  = (Rev_odom_vx * cos(Rev_odom_th) - Rev_odom_vy * sin(Rev_odom_th)) * dt;
    double delta_y  = (Rev_odom_vx * sin(Rev_odom_th) + Rev_odom_vy * cos(Rev_odom_th)) * dt;
    double delta_th = Rev_odom_w * dt;

    Rev_odom_x     += delta_x;
    Rev_odom_y     += delta_y;
    Rev_odom_th    += delta_th;

    //since all odometry is 6DOF we'll need a quaternion created from yaw
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(Rev_odom_th);

    //Publish the transform over tf
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp            = current_time;
    odom_trans.header.frame_id         = "odom";
    odom_trans.child_frame_id          = "base_link";
    //
    odom_trans.transform.translation.x = Rev_odom_x;
    odom_trans.transform.translation.y = Rev_odom_y;
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation      = odom_quat;
    //send the transform(tf)
    odom_broadcaster.sendTransform(odom_trans);

    //Publish the odometry message(topic) over ROS
    nav_msgs::Odometry odom;
    odom.header.stamp          = current_time;
    odom.header.frame_id       = "odom";

    //set the position
    odom.pose.pose.position.x  = Rev_odom_x;
    odom.pose.pose.position.y  = Rev_odom_y;
    odom.pose.pose.position.z  = 0.0;
    odom.pose.pose.orientation = odom_quat;
    //set the velocity
    odom.child_frame_id        = "base_link";
    odom.twist.twist.linear.x  = Rev_odom_vx;
    odom.twist.twist.linear.y  = Rev_odom_vy;
    odom.twist.twist.angular.z = Rev_odom_w;
    //publish the message
    odometry_Publisher_.publish(odom);

    last_time = current_time;
    */
}

float Motor::normalizeAngle(float phi)
{
    //----- Notice -----//
    // atan2(x,y) can evaluate certain angle(rad) by given (x,y)
    // Instead, atan(x) may obtains same answer with cycles(pi*k)
    //------------------//

    // Express in radians
    return atan2(sin(phi),cos(phi));
}

void Motor::readEncoder(float receive_period)
{
    /*
    int receive_package_size = 256;
    ros::Rate r_receive(1.0 / receive_period);
    while(1)
    {
        if(serial.ok)
        {
            unsigned char buff[receive_package_size];
            int readByte = 0;
            readByte = read(serial.fd,buff,receive_package_size);

            if(readByte > 0){
                for(int i=0; i<readByte; i++)
                    rev_buf.push_back(buff[i]);
            }

            if(rev_buf.size() >= 30){
                for(int i=0; i<rev_buf.size(); i++)
                {
                    if(i + (30-1) > rev_buf.size())
                        break;
                    if(rev_buf[i] == 0x0A && rev_buf[i + (30-1)] == 0x0D)
                    {
                        unsigned char FR_RPM_L = rev_buf[i+1]  + 0x00;
                        unsigned char FR_RPM_H = rev_buf[i+2]  + 0x00;

                        unsigned char FR_DEG_L = rev_buf[i+3]  + 0x00;
                        unsigned char FR_DEG_H = rev_buf[i+4]  + 0x00;

                        unsigned char FL_RPM_L = rev_buf[i+5]  + 0x00;
                        unsigned char FL_RPM_H = rev_buf[i+6]  + 0x00;

                        unsigned char FL_DEG_L = rev_buf[i+7]  + 0x00;
                        unsigned char FL_DEG_H = rev_buf[i+8]  + 0x00;

                        unsigned char RL_RPM_L = rev_buf[i+9]  + 0x00;
                        unsigned char RL_RPM_H = rev_buf[i+10] + 0x00;

                        unsigned char RL_DEG_L = rev_buf[i+11] + 0x00;
                        unsigned char RL_DEG_H = rev_buf[i+12] + 0x00;

                        unsigned char RR_RPM_L = rev_buf[i+13] + 0x00;
                        unsigned char RR_RPM_H = rev_buf[i+14] + 0x00;

                        unsigned char RR_DEG_L = rev_buf[i+15] + 0x00;
                        unsigned char RR_DEG_H = rev_buf[i+16] + 0x00;

                        unsigned char check_byte = rev_buf[i+28] + 0x00;

                        unsigned char sum_of_byte = 0;
                        for(int j=i+1; j <= (i+27); j++)
                        {
                            unsigned char byte_buf = rev_buf[j] + 0x00;
                            sum_of_byte += byte_buf;
                        }

                        sum_of_byte = sum_of_byte%256;

                        float FR_RPM = float(int(FR_RPM_H)*256 + int(FR_RPM_L));
                        float FL_RPM = float(int(FL_RPM_H)*256 + int(FL_RPM_L));
                        float RR_RPM = float(int(RR_RPM_H)*256 + int(RR_RPM_L));
                        float RL_RPM = float(int(RL_RPM_H)*256 + int(RL_RPM_L));

                        float FR_DEG = float(int(FR_DEG_H)*256 + int(FR_DEG_L));
                        float FL_DEG = float(int(FL_DEG_H)*256 + int(FL_DEG_L));
                        float RR_DEG = float(int(RR_DEG_H)*256 + int(RR_DEG_L));
                        float RL_DEG = float(int(RL_DEG_H)*256 + int(RL_DEG_L));

                        if(FR_RPM >= 32768.0)  FR_RPM = FR_RPM - 65536;
                        if(FL_RPM >= 32768.0)  FL_RPM = FL_RPM - 65536;
                        if(RR_RPM >= 32768.0)  RR_RPM = RR_RPM - 65536;
                        if(RL_RPM >= 32768.0)  RL_RPM = RL_RPM - 65536;

                        if(FR_DEG >= 32768.0)  FR_DEG = FR_DEG - 65536.0;
                        if(FL_DEG >= 32768.0)  FL_DEG = FL_DEG - 65536.0;
                        if(RR_DEG >= 32768.0)  RR_DEG = RR_DEG - 65536.0;
                        if(RL_DEG >= 32768.0)  RL_DEG = RL_DEG - 65536.0;

                        FR_RPM = FR_RPM/10;
                        FL_RPM = FL_RPM/10;
                        RR_RPM = RR_RPM/10;
                        RL_RPM = RL_RPM/10;

                        FR_DEG = FR_DEG/10;
                        FL_DEG = FL_DEG/10;
                        RR_DEG = RR_DEG/10;
                        RL_DEG = RL_DEG/10;

                        if(sum_of_byte == check_byte)
                        {
                            Rev_FR_rpm = FR_RPM;
                            Rev_FL_rpm = FL_RPM;
                            Rev_RR_rpm = RR_RPM;
                            Rev_RL_rpm = RL_RPM;

                            Rev_FR_deg = FR_DEG;
                            Rev_FL_deg = FL_DEG;
                            Rev_RR_deg = RR_DEG;
                            Rev_RL_deg = RL_DEG;
                        }
                        rev_buf.clear();
                    }
                }
            }
        }
        r_receive.sleep();
    }
    */
}
bool Motor::connectSerial(SERIAL *serial, string dev_name, speed_t baudrate)
{
    //----- Setup serial variable -----//
    serial->fd = open(SERIALPORT, O_RDWR | O_NOCTTY | O_SYNC);
    serial->baudrate = baudrate;

    //----- Check port_name -----//
    if(serial->fd < 0)
    {
        cout<<"Error opening "<<dev_name<<endl;
        return false;
    }
    //----- Check tcgetattr -----//
    if(tcgetattr(serial->fd,serial->oldtio) < 0)
    {
        cout<<"Error from setting tcgetattr"<<endl;
        return false;
    }

    //----- Setup serial option -----//
    cfsetospeed(serial->oldtio,serial->baudrate);
    cfsetispeed(serial->oldtio,serial->baudrate);

    //----- Option -----//
    serial->oldtio->c_cflag |= (CLOCAL | CREAD);
    serial->oldtio->c_cflag &= ~CSIZE;
    serial->oldtio->c_cflag |= CS8;
    serial->oldtio->c_cflag &= ~PARENB;
    serial->oldtio->c_cflag &= ~CSTOPB;
    serial->oldtio->c_cflag &= ~CRTSCTS;

    serial->oldtio->c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    serial->oldtio->c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    serial->oldtio->c_oflag &= ~OPOST;

    serial->oldtio->c_cc[VMIN] = 1;
    serial->oldtio->c_cc[VTIME] = 1;

    //----- Check tcsetattr -----//
    if(tcsetattr(serial->fd,TCSANOW,serial->oldtio) < 0)
    {
        cout<<"Error from tcsetattr"<<endl;
        return false;
    }

    cout<<"[Serial Opened]"<<endl;
    return true;
}

void Motor::sendPackage(int FL, int FLA,int BL,int BLA,int FR ,int FRA,int BR,int BRA)
{
    static unsigned char counter = 0;
    int nByte = 0;

    unsigned char command[30];
    unsigned char FLHrpm,FLLrpm, FLHdegree,FLLdegree,FRHrpm,FRFLLrpm, FRHdegree,FRLdegree
            ,BLHrpm,BLLrpm, BLHdegree,BLLdegree,BRHrpm,BRFLLrpm, BRHdegree,BRLdegree;

    if(FL < 0){
        FLHrpm=(unsigned char)((FL&0xFF00)>>8);
        FLLrpm=(unsigned char)(FL&0x00FF);
    }else{
        FLHrpm=(unsigned char)(FL/256);
        FLLrpm=(unsigned char)(FL%256);
    }
    if(FLA < 0){
        FLHdegree=(unsigned char)((FLA&0xFF00)>>8);
        FLLdegree=(unsigned char)(FLA&0x00FF);
    }else{
        FLHdegree=(unsigned char)(FLA/256);
        FLLdegree=(unsigned char)(FLA%256);
    }
    if(BL < 0){
        BLHrpm=(unsigned char)((BL&0xFF00)>>8);
        BLLrpm=(unsigned char)(BL&0x00FF);
    }else{
        BLHrpm=(unsigned char)(BL/256);
        BLLrpm=(unsigned char)(BL%256);
    }
    if(BLA < 0){
        BLHdegree=(unsigned char)((BLA&0xFF00)>>8);
        BLLdegree=(unsigned char)(BLA&0x00FF);
    }else{
        BLHdegree=(unsigned char)(BLA/256);
        BLLdegree=(unsigned char)(BLA%256);
    }
    if(FR < 0){
        FRHrpm=(unsigned char)((FR&0xFF00)>>8);
        FRFLLrpm=(unsigned char)(FR&0x00FF);
    }else{
        FRHrpm=(unsigned char)(FR/256);
        FRFLLrpm=(unsigned char)(FR%256);
    }
    if(FRA < 0){
        FRHdegree=(unsigned char)((FRA&0xFF00)>>8);
        FRLdegree=(unsigned char)(FRA&0x00FF);
    }else{
        FRHdegree=(unsigned char)(FRA/256);
        FRLdegree=(unsigned char)(FRA%256);
    }
    if(BR < 0){
        BRHrpm=(unsigned char)((BR&0xFF00)>>8);
        BRFLLrpm=(unsigned char)(BR&0x00FF);
    }else{
        BRHrpm=(unsigned char)(BR/256);
        BRFLLrpm=(unsigned char)(BR%256);
    }
    if(BRA < 0){
        BRHdegree=(unsigned char)((BRA&0xFF00)>>8);
        BRLdegree=(unsigned char)(BRA&0x00FF);
    }else{
        BRHdegree=(unsigned char)(BRA/256);
        BRLdegree=(unsigned char)(BRA%256);
    }

    command[ 0] = 0x0a;
    command[ 2] = FRHrpm;
    command[ 1] = FRFLLrpm;

    command[ 4] = FRHdegree;

    command[ 3] = FRLdegree;

    command[ 6] = FLHrpm;
    command[ 5] = FLLrpm;

    command[ 8] = FLHdegree;
    command[ 7] = FLLdegree;

    command[10] = BLHrpm;
    command[ 9] = BLLrpm;

    command[12] = BLHdegree;
    command[11] = BLLdegree;

    command[14] = BRHrpm;
    command[13] = BRFLLrpm;

    command[16] = BRHdegree;
    command[15] = BRLdegree;

    command[17] = 0x00;
    command[18] = 0x00;

    command[19] = 0x00;
    command[20] = 0x00;

    command[21] = 0x00;
    command[22] = 0x00;
    command[23] = 0x00;

    command[24] = 0x00;

    command[25] = 0x00;

    command[26] = 0x00;

    command[27] = counter;
    command[28] = 0x00;

    for(int i=1; i<=27; i++)
        command[28] += command[i];
    command[29] = 0x0d;

    if(serial->ok)
       nByte = write(serial->fd,command,30);

    counter += 1;
    if(counter > 255) counter = 0;
}

int main(int argc , char **argv)
{
    ros::init(argc,argv,"MotorNode");
    ros::NodeHandle nh;

    Motor motor(SERIALPORT,115200);
    return 0;
}
