//
// This .h file define all the function MotorNode needs.
// Histtory :
// 2021/02/22 CodePed First Beta Version
//

#ifndef _MOTOR_H_
#define _MOTOR_H_

#include <iostream>
#include <unistd.h>
#include <termio.h>
#include <fcntl.h>
#include <vector>
#include <boost/thread.hpp>
#include <time.h>


//----- Include ROS Header File -----//
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

//----- Include ROS Msg Type Header File -----//
#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/Int16.h>

using namespace std;

struct SERIAL
{
    struct termios *oldtio;
    int fd;
    int res;
    int done;
    speed_t baudrate;
    bool ok;
};

class Motor
{
    //----- Function -----//
    public:

        //----- Class Setup -----//
        Motor(string dev_name, int Baudrate);
        ~Motor();

        //----- Serial Setup -----//
        speed_t setBaudrate(int baudrate);
        bool connectSerial(SERIAL *serial,string dev_name,speed_t baudrate);

        //----- Motor Transmittion -----//
        void readEncoder(float receive_period);
        void sendPackage(int FL, int FLA,int BL,int BLA,int FR ,int FRA,int BR,int BRA);

//        //----- Timer -----//
//        void timerCallback();

        //----- Motor Control -----//
        void controlFourWheel();

        //----- Odometry -----//
        void calculateOdom();
        float normalizeAngle(float phi);

    //----- Variable -----//
    private:
        //----- ROS variable -----//
        ros::Publisher pub_CmdVel;
        ros::NodeHandle nh;
//        ros::Timer control_Timer;
//        boost::thread *receive_thread;

        //----- Timer -----//
        float samplingTime;
        ros::Timer timer_calculateOdom;
//        float time_sample;

        //----- Buffer for receiving odometry -----//
        std::vector<unsigned char>rev_buffer;

        //----- Odometry topic msgs publisher -----//
        ros::Publisher pub_odom;

        tf::TransformBroadcaster brdcast_odom;
        double Rev_odom_x,Rev_odom_y,Rev_odom_theta;
        ros::Time current_time,last_time;

        //----- Control Variable -----//
        float FL_theta,BL_theta,FR_theta,BR_theta;

        //----- Receive Motor speed control variable -----//
        float Rev_FR_rpm,Rev_FL_rpm,Rev_BR_rpm,Rev_BL_rpm;
        float Rev_FR_deg,Rev_FL_deg,Rev_BR_deg,Rev_BL_deg;

        //----- Receive Inverse Kinemetic Odometry Variable -----//
        float Rev_odom_V1,Rev_odom_V2,Rev_odom_V3,Rev_odom_V4;
        float Rev_odom_t1,Rev_odom_t2,Rev_odom_t3,Rev_odom_t4;
        float Rev_odom_vx,Rev_odom_vy,Rev_odom_v,Rev_odom_w;

        //----- Serial Variables-----//
        SERIAL *serial;
};
#endif
