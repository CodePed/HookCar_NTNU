#include "../include/Motor.h"

#include <iostream>
#include <ros/ros.h>

#define SERIALPORT "/dev/tty/USB0"

using namespace std;

int main(int argc , char **argv)
{
    ros::init(argc,argv,"MotorNode");
    ros::NodeHandle nh;

    Motor motor(SERIALPORT,115200);
    return 0;
}
