#ifndef JOYSTICK_H
#define JOYSTICK_H

#include <iostream>
#include <fcntl.h>
#include <pthread.h>
#include <math.h>
#include <linux/joystick.h>
#include <vector>
#include <unistd.h>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <geometry_msgs/Point.h>

using namespace std;


#define PUSE_BUTTON_ID 4
#define END_BUTTON_ID 5

struct joystick_position {
    float theta, r, x, y;
};

struct joystick_state {
    vector<signed short> button;
    vector<signed short> axis;
};

class Joystick
{
public:
    Joystick(char *joystick_dev);
    ~Joystick();
    void readEv();
    void RevProcess(double receive_period);
    joystick_position joystickPosition(int n);
    bool buttonPressed(int n);
    bool checkPause();
    ssize_t recvtimeout(int ss, js_event *buf, int timeout);
    
    bool active;
private:
   pthread_t thread;
   
   int joystick_fd;
   js_event *joystick_ev;
   joystick_state *joystick_st;
   __u32 version;
   __u8 axes;
   __u8 buttons;
   char name[256];

   bool isPause;
   bool joy_init;//joy-stick init: 2020-10-26

   boost::thread* receive_thread_;

   ros::NodeHandle nodeHandle_;

   ros::Publisher Point_data_publisher_;

   geometry_msgs::Point Point_data_;
   
   joystick_position command_pos;

};

#endif // JOYSTICK_H


