#include "joystick.h"

Joystick::Joystick(char *joystick_dev)
{
    active = false;

    isPause = false;//default: joy-control
    joy_init= false;

    command_pos.x = 0.0;
    command_pos.y = 0.0;
    command_pos.r = 0.0;
    command_pos.x = 0.0;

    joystick_fd = 0;
    joystick_ev = new js_event();
    joystick_st = new joystick_state();
    joystick_fd = open(joystick_dev, O_RDONLY | O_NONBLOCK);
    if (joystick_fd > 0) {
        ioctl(joystick_fd, JSIOCGNAME(256), name);
        ioctl(joystick_fd, JSIOCGVERSION, &version);
        ioctl(joystick_fd, JSIOCGAXES, &axes);
        ioctl(joystick_fd, JSIOCGBUTTONS, &buttons);
        cout << "   Name: " << name << endl;
        cout << "Version: " << version << endl;
        cout << "   Axes: " << int(axes) << endl;   // joy-axes:   6
        cout << "Buttons: " << int(buttons) << endl;// joy-button: 12

        for(int i=0; i<axes; i++)
            joystick_st->axis.push_back(0);
        for(int i=0; i<buttons; i++)
            joystick_st->button.push_back(0);

        //cout<<joystick_st->axis.size()<<endl;
        //cout<<joystick_st->button.size()<<endl;
        active = true;
    }
    Point_data_publisher_ = nodeHandle_.advertise<geometry_msgs::Point>("joystick", 10);

    receive_thread_ = new boost::thread(boost::bind(&Joystick::RevProcess, this, 0.001));
}


Joystick::~Joystick(){
    if (joystick_fd > 0) {
        active = false;
        close(joystick_fd);
    }
    delete joystick_st;
    delete joystick_ev;
    joystick_fd = 0;
}


void Joystick::readEv()
{
    ssize_t bytes = read(joystick_fd, joystick_ev, sizeof(*joystick_ev));// sizeof(*joystick_ev)=8*bytes
    if(bytes==sizeof(*joystick_ev)){ 
        joystick_ev->type &= ~JS_EVENT_INIT;
        if (joystick_ev->type & JS_EVENT_BUTTON) {
            joystick_st->button[int(joystick_ev->number)] = joystick_ev->value;
        }
        if (joystick_ev->type & JS_EVENT_AXIS) {
            joystick_st->axis[int(joystick_ev->number)]   = joystick_ev->value;
        }
        
        if(!(joy_init)){
             if(buttonPressed(PUSE_BUTTON_ID)){	
                 joy_init=true; 
                 ROS_INFO("JoyStick-Initial (default:joystick-start)");
             }
        }else{//joy_init==true
            if(buttonPressed(PUSE_BUTTON_ID)){ 
                isPause = !(isPause);
                if(isPause)
                    ROS_INFO("JoyStick Pause.");
                else
                    ROS_INFO("JoyStick Start.");
            }
            joystick_position pos_buf;
            command_pos = joystickPosition(0); 
        }                
    }
}


ssize_t Joystick::recvtimeout(int ss, js_event *buf, int timeout)
{
    fd_set fds;
    int n;
    struct timeval tv;

    FD_ZERO(&fds);
    FD_SET(ss, &fds);

    tv.tv_sec = 0;
    tv.tv_usec = timeout;

    n = select(ss+1, &fds, NULL, NULL, &tv);
    if(n == 0)    return -2; //timeout;
    if(n == -1)   return -1; //error

    return read(ss, buf, sizeof(*joystick_ev));
}


joystick_position Joystick::joystickPosition(int n) {

    joystick_position pos;
 
    if (n > -1 && n < axes) {//n: 0~6
        int i0 = n*2, i1 = n*2+1;
        float x0 = joystick_st->axis[i0]/32767.0f, y0 = -joystick_st->axis[i1]/32767.0f;
        float x  = x0 * sqrt(1 - pow(y0, 2)/2.0f), y  = y0 * sqrt(1 - pow(x0, 2)/2.0f);
        //ROS_INFO("joystick-position: x0:%f,x:%f,yo:%f,y:%f",x0,x,y0,y);
        /*if((fabs(x0)<0.01)&&(fabs(y0)<0.01)){ //noise-filter
            x0=0.0; y0=0.0; x=0.0; y=0.0;
        }*/
        pos.x = x0;
        pos.y = y0;
        pos.theta = atan2(y, x);
        pos.r = sqrt(pow(y, 2) + pow(x, 2));
    }else{
        pos.theta = pos.r = pos.x = pos.y = 0.0f;
    }
    return pos;
}


bool Joystick::buttonPressed(int n) 
{
    return n > -1 && n < buttons ? joystick_st->button[n] : 0;
}


bool Joystick::checkPause()
{
    return isPause;
}


void Joystick::RevProcess(double receive_period)
{
    int Receive_Package_Size = 32;

    ros::Rate r_receive(1.0 / receive_period);
    while(1){
        readEv();
        if(!(checkPause())){ //joy-stick start
            Point_data_.x = command_pos.x;
            Point_data_.y = command_pos.y;
            Point_data_.z = 0;
            Point_data_publisher_.publish(Point_data_);
        }
        r_receive.sleep();
    }
	
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "joystick");
    ros::Time::init();

    Joystick *js;
    if(argc < 2){
        ROS_INFO("usage: <Joystick Device Name> ");
        return 0;
    }else{
        js = new Joystick(argv[1]);
        if(js->active == false){
            ROS_INFO("Open-joy failed ");
            return 0;
        }
    }
    ROS_INFO("joystick-node Start.");
    ros::spin();

    return 0;
}
